/**
 * Part 2 of the assignment
 */

package week4;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class HTTP {

    public static void main(String args) throws Exception {

        //URL object that references the webpage that I grab the JSON from
        URL webPage = new URL("https://www.imdb.com/title/tt0076759/");

        //connection object that opens the connection to the webpage
        HttpURLConnection pageView = (HttpURLConnection) webPage.openConnection();

        //reader option that gets the webpage
        BufferedReader getPage = new BufferedReader(new InputStreamReader(pageView.getInputStream()));

        String http; //create a string variable for http

        //part of output seen by the user. Helps organize what is shown
        System.out.println("--END OF VALUES--");
        System.out.println("-----------------");
        System.out.println("                                                                       ");
        System.out.println("                                                                       ");
        System.out.println("JSON Object from the StarWars page on IMDb (below)");
        System.out.println("=======================================================================");

        //while loop that gets and reads the page based on if there is something to read
        while((http = getPage.readLine()) != null){
            if(http.isEmpty() != true){
                System.out.println(http);
            }else {
                System.out.println("                    ");
            }
        }
        getPage.close();
    }
}
