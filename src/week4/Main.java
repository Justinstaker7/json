/**
 * Part one of the assignment
 */

package week4;
import java.io.*;
import java.util.Scanner;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.util.JSONPObject;

//I got a lot of help from the instructional video
//I took a lot of the logic from the video and put it in an order that makes sense to me
//Thanks for your help each week!
//

public class Main {

    public static void main(String[] args) throws Exception{

        //create object for Movies
        Movies ben = new Movies();

        //call Movies class to return values as JSON block
        ben.setMovieName("StarWars"); //String MovieName
        ben.setDirector("George Lucas"); //String Director
        ben.setReleaseYear(1977); //long ReleaseYear
        ben.setBoxOffice$$(775398007); //long BoxOffice$$ (total global movie earnings)

        String getJSON = Main.MoviesToJSON(ben); System.out.println(getJSON);
        Movies leia = Main.JSONToMovies(getJSON); System.out.println(leia);

        //calls HTTP class to output JSON object to user
        HTTP chewbacca = new HTTP();
        chewbacca.main("https://www.imdb.com/title/tt0076759/");

    }

    //method to translate movies to JSON
    public static String MoviesToJSON(Movies StarWars) throws JsonProcessingException{

        //mapper object using Jackson ObjectMapper
        ObjectMapper luke = new ObjectMapper();

        String xWing = "";
        System.out.println("                    ");
        System.out.println("==JSON Block=="); //inform user this is where JSON block is located
        System.out.println("--------------------"); //line divider to organize output

        //try catch block
        //try- write objectMapper as a string
        //catch- if Json processing does not work, output error
        try {
                xWing = luke.writeValueAsString(StarWars);
        } catch (JsonProcessingException e) {
            System.out.println("error: this is not the JSON you're looking for");
        }

        return xWing;
    }

    //method to translate JSON to movies
    public static Movies JSONToMovies(String falcon) throws Exception {

        //second object using Jackson ObjectMapper
        ObjectMapper han = new ObjectMapper();
        Movies StarWars = null;

        //try catch block
        //try- read the String variable that was written by prev method
        //catch- if Json processing does not work, output error
        try{
            StarWars = han.readValue(falcon, Movies.class);
        }catch (JsonProcessingException e){
            System.out.println("error: this is not the JSON you're looking for");
        }

        return StarWars;
    }

}