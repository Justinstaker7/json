//Movies class
//Values are StarWars themed
//
package week4;

public class Movies {

    private String movieName;
    private String director;
    private long releaseYear;
    private long boxOffice$$;

    //first variable movieName
    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName){

        this.movieName = movieName;
    }

    //second variable director
    public String getDirector() {
        return director;
    }

    public void setDirector(String director){

        this.director = director;
    }

    //third variable releaseYear
    public long getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(long releaseYear){

        this.releaseYear = releaseYear;
    }

    //fourth variable boxOffice$$
    public long getBoxOffice$$() {
        return boxOffice$$;
    }

    public void setBoxOffice$$(long boxOffice$$){

        this.boxOffice$$ = boxOffice$$;
    }

    //to string method to output
    public String toString(){
        System.out.println("                    ");
        System.out.println("                    ");
        System.out.println("==Values Used==");
        System.out.println("--------------------");
        return "Movie: " + movieName + "\nDirected by: " + director +
                "\nYear Released: " + releaseYear + "\nEarnings: " + boxOffice$$;
    }
}
